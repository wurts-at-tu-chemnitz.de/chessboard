function topK(list, k) {
    list.sort((a, b) => b - a);
    return list.slice(0, k);
}

function sum(start, end) {
    // Formula for the sum of an arithmetic series: n/2 * (first term + last term)
    return ((end - start + 1) / 2) * (start + end);
}

function flatten(list) {
    let flatList = [];

    function flattenElement(element) {
        if (Array.isArray(element)) {
            element.forEach(flattenElement);
        } else {
            flatList.push(element);
        }
    }

    list.forEach(flattenElement);
    return flatList;
}
