import express from "express";
const router = express.Router();
import grpcClient from "../grpc/grpc.js";

//import chessboardServerInstance from "../services/ChessboardServer.js";
import { grpcCallWrapper } from "../utils/grpc.js";

import {
    userHandlerMiddleware,
    userUuidMiddleware,
} from "../middleware/user.js";

router.use(userUuidMiddleware);
router.use(userHandlerMiddleware);

router.get("/startGame", (req, res) => {
    const chessboardServerInstance = req.userSession;

    console.log("uuid: " + req.userUuid);

    chessboardServerInstance.setFen(
        "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
    );
    console.log("fen: " + chessboardServerInstance.getFen());
    res.json({ fen: chessboardServerInstance.getFen() });
});

router.get("/getGameState", (req, res) => {
    const chessboardServerInstance = req.userSession;

    /* console.log("instance: " + JSON.stringify(chessboardServerInstance));
    console.log("uuid: " + req.userUuid);
    console.log("fen: " + chessboardServerInstance.getFen()); */

    res.json({ fen: chessboardServerInstance.getFen() });
});

router.post("/getPossibleMoves", (req, res) => {
    const { selectedPieceId } = req.body;
    const chessboardServerInstance = req.userSession;

    const requestGrpc = {
        current_board: { fen: chessboardServerInstance.getFen() },
        from: selectedPieceId,
    };

    grpcCallWrapper(grpcClient.GetPossibleMoves, requestGrpc, (result) => {
        const possibleMoves = result.data.possible_moves;

        chessboardServerInstance.setSelectedPieceToMove(
            selectedPieceId,
            possibleMoves
        );

        console.log("possibleMoves: " + JSON.stringify(result.data));

        const possibleMovesParsed = possibleMoves?.map((move) => {
            return move.to;
        });

        res.status(result.status).json(possibleMovesParsed);
    });
});

router.post("/validateMove", (req, res) => {
    const chessboardServerInstance = req.userSession;

    const idOfPieceToValidate = req.body.moveToValidate;

    const moveExistsInServer = chessboardServerInstance
        .getSelectedPieceToMove()
        .possibleMoves.find((move) => move.to === idOfPieceToValidate);

    if (!moveExistsInServer) {
        res.status(400).json({
            message: "Invalid move",
        });
    }

    const requestGrpc = { move_to_validate: moveExistsInServer };

    grpcCallWrapper(grpcClient.Validate, requestGrpc, (result) => {
        const newBoardFen = result.data.validated_move.board_after_move.fen;
        const newBoardStatus = result.data.validated_move.status;

        chessboardServerInstance.resetSelectedPieceToMove();
        chessboardServerInstance.setFen(newBoardFen);
        res.status(result.status).json({
            fen: newBoardFen,
            status: newBoardStatus,
        });
    });
});

router.get("/getOponentMove", (req, res) => {
    const chessboardServerInstance = req.userSession;

    const requestGrpc = {
        current_board: { fen: chessboardServerInstance.getFen() },
    };

    grpcCallWrapper(grpcClient.Play, requestGrpc, (result) => {
        console.log("result: " + JSON.stringify(result));
        const newBoardFen = result.data.move_made.board_after_move.fen;
        const newBoardStatus = result.data.move_made.status;
        console.log("newBoardFen: " + newBoardFen);
        //chessboardServerInstance.resetSelectedPieceToMove();
        chessboardServerInstance.setFen(newBoardFen);
        res.status(result.status).json({
            fen: newBoardFen,
            status: newBoardStatus,
        });
    });
});

export default router;
