import express from "express";
const router = express.Router();
import { renderChessboard } from "../utils/render.js";

router.get("/", function (req, res, next) {
    let chessboard = renderChessboard("white");

    res.render("chessboard", {
        title: "Chessboard Game",
        chessboardHTML: chessboard,
    });
});

export default router;
