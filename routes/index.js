import express from "express";
const router = express.Router();
import { renderChessboard } from "../utils/render.js";

/* GET home page. */
router.get("/", function (req, res, next) {
    res.render("index", {
        title: "Chessboard - Homepage",
    });
});

export default router;
