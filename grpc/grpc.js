import grpc from "@grpc/grpc-js";
import protoLoader from "@grpc/proto-loader";

const packageDefinition = protoLoader.loadSync(
    "grpc/protos/move_generator.proto",
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true,
    }
);
const moveGeneratorProto =
    grpc.loadPackageDefinition(packageDefinition).move_generator;

// Initialize and export the gRPC client
const grpcClient = new moveGeneratorProto.MoveGenerator(
    "vsrstud02.informatik.tu-chemnitz.de:50051",
    grpc.credentials.createInsecure()
);

// Create a new object that wraps the gRPC client and binds all methods to the client
const boundGrpcClient = {};

for (const method in grpcClient) {
    if (typeof grpcClient[method] === "function") {
        boundGrpcClient[method] = grpcClient[method].bind(grpcClient);
    }
}

export default boundGrpcClient;
