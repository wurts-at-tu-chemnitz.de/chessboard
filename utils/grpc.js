/**
 * Wrapper function for making gRPC calls.
 *
 * @param {Function} method - The gRPC method to call.
 * @param {Object} request - The request object to send.
 * @param {Function} callback - The callback function to handle the response.
 * @param {boolean} [logOutput=true] - Whether to log the output to the console.
 */
export function grpcCallWrapper(method, request, callback, logOutput = false) {
    try {
        method(request, (error, response) => {
            if (error) {
                if (logOutput) {
                    console.log("Error calling gRPC service:");
                    console.error(error);
                }
                callback({
                    status: 500,
                    data: { message: "Error calling gRPC service", error },
                });
            } else {
                if (logOutput) {
                    console.log("Response received from gRPC server:");
                    console.log(response);
                }
                callback({ status: 200, data: response });
            }
        });
    } catch (e) {
        console.log("grpcCallWrapperError", e);
    }
}
