/**
 * Generates the HTML for a chessboard from a given perspective.
 *
 * @param {string} pov - The perspective of the user ("white" or "black").
 * @returns {string} The HTML string representing the chessboard.
 */
export function renderChessboard(pov) {
    const rows = ["8", "7", "6", "5", "4", "3", "2", "1"];
    const cols = ["A", "B", "C", "D", "E", "F", "G", "H"];

    if (pov === "black") {
        rows.reverse();
        cols.reverse();
    }

    let html = '<div class="chessboard">';

    rows.forEach((row) => {
        cols.forEach((col) => {
            const chessId = `${col}${row}`;
            html += `<div id="${chessId}">
                        <span class="chessIdLabel">${chessId}</span>
                        <span class="chesspiece"></span>
                        <span class="chessPieceOverlay"></span>
                    </div>`;
        });
    });

    html += "</div>";

    return html;
}
