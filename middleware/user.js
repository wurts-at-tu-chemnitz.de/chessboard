import userHandler from "../services/UserHandler.js";
import { ChessboardServer } from "../services/ChessboardServer.js";

export const userHandlerMiddleware = (req, res, next) => {
    console.log("userHandlerMiddleware");
    const userUuid = req.userUuid;

    let userSession = userHandler.getSession(userUuid);

    if (!userSession) {
        console.log("Creating new user session");
        userSession = new ChessboardServer();
        userHandler.createSession(userUuid, userSession);
    }

    console.log("userSession: " + JSON.stringify(userSession.getFen()));

    req.userSession = userSession;
    next();
};

export const userUuidMiddleware = (req, res, next) => {
    console.log("userUuidMiddleware");
    let userUuid = req.cookies.userUuid;
    console.log("userUuidMiddleware2", userUuid);

    if (!userUuid) {
        userUuid = Math.random().toString(36).substring(2, 15);
        console.log("userUuidMiddleware3", userUuid);

        res.cookie("userUuid", userUuid);
    }
    console.log("userUuid4: " + userUuid);
    req.userUuid = userUuid;
    next();
};
