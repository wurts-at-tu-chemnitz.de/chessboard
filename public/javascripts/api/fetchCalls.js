import { fetchCallWrapper } from "../utils/fetch.js";

/**
 *
 * @returns {Promise} - A promise that resolves to the FEN string.
 */
export async function startGame() {
    return await fetchCallWrapper(
        fetch("/api/gameEngine/startGame", {
            method: "GET",
        })
    );
}

/**
 *
 * @returns {Promise} - A promise that resolves to the FEN string.
 */
export async function getGameState() {
    console.log("getGameState");
    return await fetchCallWrapper(
        fetch("/api/gameEngine/getGameState", {
            method: "GET",
        })
    );
}

/**
 * Fetches the possible moves for a given piece on the current board.
 * @param {string} selectedPieceId - The piece to get the possible moves for.
 * @returns {Promise} - A promise that resolves to the possible moves.
 */
export async function getPossibleMoves(selectedPieceId) {
    return await fetchCallWrapper(
        fetch("/api/gameEngine/getPossibleMoves", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ selectedPieceId }),
        })
    );
}

/**
 * Validates a move on the chessboard.
 * @param {Object} move - The move object containing the necessary information.
 * @param {string} move.board_before_move - The board state before the move.
 * @param {string} move.board_after_move - The board state after the move.
 * @param {string} move.from - The square index of the piece to move.
 * @param {string} move.to - The square index to move the piece to.
 * @param {string} move.status - The status of the game after the move.
 * @returns {Promise} - A promise that resolves to the validation result.
 */
export async function validateMove(moveToValidate) {
    return await fetchCallWrapper(
        fetch("/api/gameEngine/validateMove", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ moveToValidate }),
        })
    );
}

export async function getOponentMove() {
    return await fetchCallWrapper(
        fetch("/api/gameEngine/getOponentMove", {
            method: "GET",
        })
    );
}
