/**
 * Wraps a fetch call with error handling.
 * @param {Promise<Response>} fetchCall - The fetch call to be wrapped.
 * @returns {Promise<any>} - The response data from the fetch call.
 */
export async function fetchCallWrapper(fetchCall) {
    try {
        const response = await fetchCall;

        if (!response.ok) {
            throw new Error(`HTTP error! status: ${response.status}`);
        }

        const data = await response.json();
        return data;
    } catch (error) {
        console.error("Error fetching:", error);
    }
}
