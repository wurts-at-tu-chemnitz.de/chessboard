import {
    getPossibleMoves,
    validateMove,
    startGame,
    getOponentMove,
    getGameState,
} from "./api/fetchCalls.js";
import { CHESSBOARD_SQUARES } from "./utils/boardConstants.js";

export class ChessboardClient {
    /**
     * Represents a ChessboardClient object.
     * @constructor
     */
    constructor(isGameClient = true) {
        if (isGameClient) {
            this.clientColor = "White";
            this.fen = null;
            this.turn = null;
            this.piecesInTheBoard = {};
            this.boardHTMLElements = {};
            //this.boardHTMLElements = this.getBoardHTMLElements();
            this.selectedPieceToMove = null;
        }
    }

    /**
     * Tells the server to start a new game session
     * - Called when new game button is clicked
     */
    async startGame() {
        console.log("startGame");
        await startGame();
    }

    /**
     * Loads the game state from the server
     * - Called when the game page loads
     */
    async loadGame() {
        this.boardHTMLElements = this.getBoardHTMLElements();
        const { fen } = await getGameState();
        this.resetBoardFromFen(fen);
    }

    resetBoardFromFen(fen) {
        this.resetBoardState();
        this.fen = fen;
        const parsedFen = this.parseFENString();
        this.piecesInTheBoard = parsedFen.pieces;
        this.turn = parsedFen.turn;
        this.renderCurrentTurn();
        this.renderBoardPieces();
        if (this.clientColor === this.turn) {
            this.attachOnSelectGetPossibleMoves();
        }
    }

    async requestOpponentMove() {
        if (this.clientColor !== this.turn) {
            // Add non blocking delay otherwise server is to fast
            await new Promise((resolve) => setTimeout(resolve, 1000));

            const newBoardToRenderFen = await getOponentMove();
            console.log("openent status: " + newBoardToRenderFen.status);
            if (newBoardToRenderFen) {
                this.resetBoardFromFen(newBoardToRenderFen.fen);
            }
        }
    }

    attachOnSelectValidateMove() {
        this.selectedPieceToMove.possibleMoves.forEach((possibleMoveId) => {
            this.boardHTMLElements[possibleMoveId].overlay.onclick =
                async () => {
                    const newBoardToRenderFen = await validateMove(
                        possibleMoveId
                    );

                    console.log("status: " + newBoardToRenderFen.status);

                    if (newBoardToRenderFen) {
                        this.resetBoardFromFen(newBoardToRenderFen.fen);
                        await this.requestOpponentMove();
                    }
                };
        });
    }

    attachOnSelectGetPossibleMoves() {
        Object.keys(this.piecesInTheBoard[this.clientColor]).forEach(
            (pieceId) => {
                this.renderOverlayAsSelectable(pieceId);

                this.boardHTMLElements[pieceId].overlay.onclick = async () => {
                    // If we already have a piece selected, we reset the board overlays and reattach the event listeners.
                    if (this.selectedPieceToMove) {
                        this.resetBoardOverlays();
                        this.attachOnSelectGetPossibleMoves();
                    }

                    const possibleMovesForPiece = await getPossibleMoves(
                        pieceId
                    );

                    this.selectedPieceToMove = {
                        id: pieceId,
                        possibleMoves: possibleMovesForPiece,
                    };

                    this.renderOverlayAsSelected(pieceId);
                    this.renderOverlayPossibleMoves(possibleMovesForPiece);
                    this.attachOnSelectValidateMove();
                };
            }
        );
    }

    resetBoardState() {
        this.resetBoardPieces();
        this.resetBoardOverlays();
        this.piecesInTheBoard = {};
        this.selectedPieceToMove = null;
    }

    resetBoardPieces() {
        Object.values(this.boardHTMLElements).forEach((elements) => {
            elements.piece.className = "chesspiece";
            elements.piece.textContent = "";
        });
    }

    resetBoardOverlays() {
        Object.values(this.boardHTMLElements).forEach((elements) => {
            elements.overlay.className = "chessPieceOverlay";
            elements.overlay.onclick = null;
        });
    }

    renderCurrentTurn() {
        const player = document.getElementById("user-row-player");
        const oponent = document.getElementById("user-row-oponent");
        if (this.turn === "White") {
            player.className = "user-row user-row-active";
            oponent.className = "user-row";
        } else {
            player.className = "user-row";
            oponent.className = "user-row user-row-active";
        }
    }

    renderOverlayAsSelected(overlayId) {
        const { overlay } = this.boardHTMLElements[overlayId];
        overlay.className = "chessPieceOverlay chessPieceOverlaySelected";
    }

    renderOverlayAsSelectable(overlayId) {
        const { overlay } = this.boardHTMLElements[overlayId];
        overlay.className = "chessPieceOverlay chessPieceOverlayHover";
    }

    renderOverlayAsPossible(overlayId) {
        const { overlay } = this.boardHTMLElements[overlayId];
        overlay.className = "chessPieceOverlay chessPieceOverlayPossible";
    }

    renderOverlayPossibleMoves(possibleMovesIds) {
        possibleMovesIds.forEach((possibleMoveId) => {
            this.renderOverlayAsPossible(possibleMoveId);
        });
    }

    renderBoardPieces() {
        const whitePiecesToRender = Object.values(this.piecesInTheBoard.White);
        whitePiecesToRender.forEach((pieceToRender) => {
            const { piece: chessPiece } =
                this.boardHTMLElements[pieceToRender.cellId];

            chessPiece.className =
                "chesspiece chesspiece" + pieceToRender.color;
            chessPiece.textContent = pieceToRender.unicode;
        });
        const blackPiecesToRender = Object.values(this.piecesInTheBoard.Black);
        blackPiecesToRender.forEach((pieceToRender) => {
            const { piece: chessPiece } =
                this.boardHTMLElements[pieceToRender.cellId];

            chessPiece.className =
                "chesspiece chesspiece" + pieceToRender.color;
            chessPiece.textContent = pieceToRender.unicode;
        });
    }

    getBoardHTMLElements() {
        const boardHTMLElements = {};
        Object.keys(CHESSBOARD_SQUARES).forEach((boardCellId) => {
            boardHTMLElements[boardCellId] =
                this.getHTMLElementsForId(boardCellId);
        });
        return boardHTMLElements;
    }

    getHTMLElementsForId(id) {
        const chessPieceSquare = document.getElementById(id);
        const chessPieceIdLabel =
            chessPieceSquare.querySelector(".chessIdLabel");
        const chessPiece = chessPieceSquare.querySelector(".chesspiece");
        const chessPieceOverlay =
            chessPieceSquare.querySelector(".chessPieceOverlay");

        return {
            square: chessPieceSquare,
            idLabel: chessPieceIdLabel,
            piece: chessPiece,
            overlay: chessPieceOverlay,
        };
    }

    parseFENString() {
        const fenParts = this.fen.split(" ");
        const fenBoard = fenParts[0];
        const turn = fenParts[1] === "w" ? "White" : "Black";
        const rows = fenBoard.split("/");

        const tempPiecesInTheBoard = {
            Black: {},
            White: {},
        };

        const fenToUnicode = {
            P: "♙",
            R: "♖",
            N: "♘",
            B: "♗",
            Q: "♕",
            K: "♔",
        };

        rows.forEach((row, rowIndex) => {
            let columnIndex = 0;
            for (let char of row) {
                if (isNaN(char)) {
                    const pieceUnicode = fenToUnicode[char.toUpperCase()];
                    const pieceColor =
                        char === char.toLowerCase() ? "Black" : "White";
                    const pieceCellId =
                        String.fromCharCode(65 + columnIndex) + (8 - rowIndex);

                    tempPiecesInTheBoard[pieceColor][pieceCellId] = {
                        cellId: pieceCellId,
                        char: char,
                        unicode: pieceUnicode,
                        color: pieceColor,
                    };

                    columnIndex++;
                } else {
                    columnIndex += parseInt(char);
                }
            }
        });

        return {
            pieces: tempPiecesInTheBoard,
            turn: turn,
        };
    }
}

const chessboardClient = new ChessboardClient();

export default chessboardClient;
