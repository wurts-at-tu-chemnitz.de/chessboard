class UserHandler {
    constructor() {
        this.sessions = {};
    }

    getSessions() {
        return this.sessions;
    }

    getSession(id) {
        return this.sessions[id];
    }

    createSession(id, session) {
        this.sessions[id] = session;
    }

    deleteSession(id) {
        delete this.sessions[id];
    }
}

// Initialize the singleton
const userHandlerInstance = new UserHandler();
/* "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" */

export default userHandlerInstance;
