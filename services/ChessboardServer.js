export class ChessboardServer {
    constructor(fen = null) {
        this.fen = fen;
        this.selectedPieceToMove = null;
    }

    getFen() {
        return this.fen;
    }

    setFen(fen) {
        this.fen = fen;
    }

    getSelectedPieceToMove() {
        return this.selectedPieceToMove;
    }

    setSelectedPieceToMove(selectedPieceToMove, possibleMoves) {
        this.selectedPieceToMove = {
            id: selectedPieceToMove,
            possibleMoves,
        };
    }

    resetSelectedPieceToMove() {
        this.selectedPieceToMove = null;
    }
}

// Initialize the singleton
// chessboardServerInstance = new ChessboardServer();
/* "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1" */

// export default chessboardServerInstance;
